/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/Users/jasminexu/Desktop/seal/BME_280_test/src/BME_280_test.ino"
/***************************************************************************
  This is a library for the BME280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME280 Breakout
  ----> http://www.adafruit.com/products/2650

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface. The device's I2C address is either 0x76 or 0x77.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
  See the LICENSE file for details.
 ***************************************************************************/

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include"/Users/jasminexu/Desktop/seal/BME_280_test/lib/pmsa_i2c/src/pmsa_i2c.h"
#include "Adafruit_EPD_RK.h"
#include "SdFat.h"
#include "Adafruit_GPS.h"

void setup();
void loop();
void printValues();
void displayInit();
void gps_Init();
void gps_READ();
void write_SD();
void displayDATA();
void updateData();
#line 28 "/Users/jasminexu/Desktop/seal/BME_280_test/src/BME_280_test.ino"
#define SD_CS D2
#define SRAM_CS D3
#define EPD_CS D4
#define EPD_DC D5



#define SEALEVELPRESSURE_HPA (1013.25)




#define EPD_RESET -1 // can set to -1 and share with microcontroller Reset!
#define EPD_BUSY -1  // can set to -1 to not use a pin (will wait a fixed delay)

#define ORIGIN_X 10
#define ORIGIN_Y 10

/* GPS Definitions  */
#define GPSSerial Serial1

Adafruit_GPS GPS(&GPSSerial);

/* Screen obj */
Adafruit_IL0373 epd(212, 104, EPD_DC, EPD_RESET, EPD_CS, SRAM_CS, EPD_BUSY);

/* Particle Sensor obj */
PMSA003 pm = PMSA003(); // create instance of class

SdFat SD;
File myFile;

/* Screen Refresh rate */
const int REDRAW_DELAY = 30000; // 30 Seconds
unsigned long preSensorDutyMillis;

/* convert UTC to local time */
int difference = GPS.longitudeDegrees / 15;
int hourNew = GPS.hour + difference;

/* time counter for each 30 sec */
int n;
/* A string Array to store 10 mins data*/
String mvpData[10];


Adafruit_BME280 bme; // I2C
//Adafruit_BME280 bme(BME_CS); // hardware SPI
//Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

unsigned long delayTime;

void setup()
{
    Serial.begin(115200);
    while (!Serial)
        ; // time to get serial running

    unsigned status;
  
  delay(1000);
  //while (!Serial);
  SD.begin(SD_CS);
  //{
  //  Serial.println("failed!");
  //}

  delay(1000);
  GPS.begin(9600);
  pm.begin();
  epd.begin();
  displayInit();
  gps_Init();
  delay(1000);
  Serial.println("***********");
  n = 0;
    // default settings
    // (you can also pass in a Wire library object like &Wire2)
    status = bme.begin();
    if (!status)
    {
        Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
        Serial.print("SensorID was: 0x");
        Serial.println(bme.sensorID(), 16);
        Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
        Serial.print("   ID of 0x56-0x58 represents a BMP 280,\n");
        Serial.print("        ID of 0x60 represents a BME 280.\n");
        Serial.print("        ID of 0x61 represents a BME 680.\n");
        while (1)
            ;
    }

    Serial.println("-- Default Test --");
    delayTime = 1000;

    Serial.println();
}

void loop()
{
    /******************************************************/
  gps_READ(); // Must be called as frequently as possible, at least faster than the set read freqency. Must be placed in timer interupt in future.
  /******************************************************/
 if (!preSensorDutyMillis)
  {
    preSensorDutyMillis = millis();
  }

  unsigned long curSensorDutyMillis = millis();
  if (curSensorDutyMillis - preSensorDutyMillis >= REDRAW_DELAY)
  {
    // Grab PM data from I2C bus, store in buffer
    pm.poll();
    Serial.println("++++++++++++++++++++++++++++++");
    // Write to SD card
    write_SD();
    // Call display data block 
    displayDATA();

    printValues();
    // publish the past 10 mins data to the console, with 1 min sampling rate
    updateData(); 
    // If past duty time, reset timer
    preSensorDutyMillis = curSensorDutyMillis;
  } 

    delay(delayTime);
}












void printValues()
{
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();
}

// The initial screen on E-paper display
void displayInit(){
  epd.setTextWrap(true);
  epd.setTextSize(2);
  epd.clearBuffer();
  epd.setCursor(10, 10);
  epd.setTextColor(EPD_BLACK);
  epd.print("Project Aerospec:");
  epd.setCursor(10, 60);
  epd.setTextColor(EPD_RED);
  epd.print("MVP V1.0");
  epd.display();
  Serial.println("Initialized");
}

void gps_Init()
{
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_ALLDATA);   // Sets to GPS Pos, Quality and time mode only
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);       // 1 second read frequency
}


void gps_READ()
{

  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
  // if a sentence is received, we can check the checksum, parse it...
  if (GPS.newNMEAreceived())
  {
    // a tricky thing here is if we print the NMEA sentence, or data
    // we end up not listening and catching other sentences!
    // so be very wary if using OUTPUT_ALLDATA and trying to print out data
    //Serial.println(GPS.lastNMEA()); // this also sets the newNMEAreceived() flag to false
    if (!GPS.parse(GPS.lastNMEA())) // this also sets the newNMEAreceived() flag to false
      return;                       // we can fail to parse a sentence in which case we should just wait for another
  }
}

// Write the air quality data from the pm sensor to the SD card
void write_SD(){
    myFile = SD.open("test.txt", FILE_WRITE);
  if (myFile) {
    Serial.println("test.txt:");
    Serial.print("Writing to test.txt...");
    myFile.println();
    myFile.print("PM0 0.3/.1L air: **");
    myFile.print(pm.pt_03());
    myFile.println();

    myFile.print("PM0 0.5/.1L air: **");
    myFile.print(pm.pt_05());
    myFile.println();

    myFile.print("PM0 1.0/.1L air: **");
    myFile.print(pm.pt_10());
    myFile.println();

    myFile.print("PM0 2.5/.1L air: **");
    myFile.print(pm.pt_25());
    myFile.println();

    myFile.print("PM0 5.0/.1L air: **");
    myFile.print(pm.pt_50());
    myFile.println();

    myFile.print("PM0 10.0/.1L air: **");
    myFile.print(pm.pt_100());
    myFile.println();

    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

}


void displayDATA()
{
  epd.setTextWrap(true);
  epd.setTextSize(1);
  epd.clearBuffer();
  /* Prints Time */
  epd.setCursor(ORIGIN_X, 10);
  epd.setTextColor(EPD_BLACK);
  epd.print(hourNew);
  epd.print(":");
  epd.print(GPS.minute);
  epd.print(":");
  epd.print(GPS.seconds);


  // Serial prints time
  Serial.print(GPS.hour);
  Serial.print(":");
  Serial.print(GPS.minute);
  Serial.print(":");
  Serial.print(GPS.seconds);
  Serial.println("difference : ");
  Serial.print(difference);
  Serial. println();

  

  /* Prints PM Sensor Data */
  epd.setCursor(ORIGIN_X, 20);
  epd.setTextColor(EPD_BLACK);
  epd.print("PM0 0.3/.1L air: ");
  epd.print(pm.pt_03());

  epd.setCursor(ORIGIN_X, 30);
  epd.print("PM0 0.5/.1L air: ");
  epd.print(pm.pt_05());

  epd.setCursor(ORIGIN_X, 40);
  epd.print("PM0 1.0/.1L air: ");
  epd.print(pm.pt_10());

  epd.setCursor(ORIGIN_X, 50);
  epd.print("PM0 2.5/.1L air: ");
  epd.print(pm.pt_25());

  epd.setCursor(ORIGIN_X, 60);
  epd.print("PM0 5.0/.1L air: ");
  epd.print(pm.pt_50());

  epd.setCursor(ORIGIN_X, 70);
  epd.print("PM0 10.0/.1L air: ");
  epd.print(pm.pt_100());


    /* Serial Prints PM Sensor Data */
  Serial.print("PM0 0.3/.1L air: ");
  Serial.print(pm.pt_03());
  Serial.println();

  Serial.print("PM0 0.5/.1L air: ");
  Serial.print(pm.pt_05());
  Serial.println();

  Serial.print("PM0 1.0/.1L air: ");
  Serial.print(pm.pt_10());
  Serial.println();

  Serial.print("PM0 2.5/.1L air: ");
  Serial.print(pm.pt_25());
  Serial.println();

  Serial.print("PM0 5.0/.1L air: ");
  Serial.print(pm.pt_50());
  Serial.println();

  Serial.print("PM0 10.0/.1L air: ");
  Serial.print(pm.pt_100());
  Serial.println();


  /* Prints GPS data */
  epd.setCursor(ORIGIN_X, 80);
  epd.print("LAT:");
  epd.print(GPS.latitudeDegrees);
  epd.print("  LONG:");
  epd.print(GPS.longitudeDegrees);
  epd.print("  FIX:");
  epd.print(GPS.fixquality);

  epd.display();
}


// publish data into the console in 10 mins rate with 1 min sampling rate
void updateData()
 {
   String GPSData = "GPS " + String(GPS.latitudeDegrees) + "," + String(GPS.longitudeDegrees);
  String airQuality = " Air Quality : 【 " +  String(pm.pt_03()) + " ， " + String(pm.pt_05()) + + " ， " + String(pm.pt_10())
                     + " ， " + String(pm.pt_25()) + " ， " + String(pm.pt_50()) +  " ， " + String(pm.pt_100()) + " 】 ";
  String dataCurr = "  " + String(n / 2) + airQuality;
  mvpData[n / 2] = dataCurr;
  
   if (n < 18){
     n += 1;
   }else{
     Particle.publish("current_data", GPSData + mvpData[0] + "  " 
                      + mvpData[1] + "  " + mvpData[2] + "  " + mvpData[3] + "  "
                      + mvpData[4] + "  " + mvpData[5] + "  " + mvpData[6] + "  "
                      + mvpData[7] + "  " + mvpData[8] + "  "+ mvpData[9] + "  "
                      , 600, PRIVATE);
     n = 0;
   }
 }

