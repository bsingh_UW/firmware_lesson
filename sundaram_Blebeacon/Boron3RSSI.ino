/*
 * Project Boron3RSSI
 * Description:
 * Author:
 * Date:
 */


#include "Particle.h"

//To display log messages using serial monitor.
SerialLogHandler logHandler(LOG_LEVEL_INFO);

//Create BleScanResult object of specified max length
const size_t SCAN_RESULT_MAX = 30;
BleScanResult scanResults[SCAN_RESULT_MAX];

//Create BleAddress object for every estimote beacon
const uint8_t BLE_BEACON_1[BLE_SIG_ADDR_LEN] = {0x6E, 0xFD, 0xFC, 0x9D, 0xF2,0xDD};
BleAddress bleAddress1 (BLE_BEACON_1,BleAddressType::PUBLIC);

const uint8_t BLE_BEACON_2[BLE_SIG_ADDR_LEN] = {0xF9, 0x7C, 0x86, 0x0F, 0x1A, 0xFC};
BleAddress bleAddress2 (BLE_BEACON_2,BleAddressType::PUBLIC);

const uint8_t BLE_BEACON_3[BLE_SIG_ADDR_LEN] = {0x15, 0x42, 0x07, 0x79, 0xD8, 0xCD};
BleAddress bleAddress3 (BLE_BEACON_3,BleAddressType::PUBLIC);

//const BleAddress bleAddress1((char*)("6EFDFC9DF2DD"),BleAddressType::PUBLIC);
//const BleAddress bleAddress2((char*)("F97C860F1AFC"),BleAddressType::PUBLIC);
//const BleAddress bleAddress3((char*)("15420779D8CD"),BleAddressType::PUBLIC);

void setup() {
  
  (void) logHandler;
  Serial.begin();
  //turn on BLe
  BLE.on();
}


void loop() {
  
  Serial.printlnf("Scanning...");
  //Set how long scan will run. Here set to scan for every 500 millisec.
  BLE.setScanTimeout(50);

  int count = BLE.scan(scanResults, SCAN_RESULT_MAX);

  //Iterate through number of devices found and if match found for estimote beacons, read the rssi.
  for(int ii=0; ii<count; ii++){
    /*Serial.printlnf("index: %2i RSSI: %d  MAC: %02X:%02X:%02X:%02X:%02X:%02X ",
        ii, scanResults[ii].rssi, 
        scanResults[ii].address[0], scanResults[ii].address[1], scanResults[ii].address[2],
        scanResults[ii].address[3], scanResults[ii].address[4], scanResults[ii].address[5]);*/
    //for(int jj=0; jj<6; jj++){
          
      if(scanResults[ii].address[0] == bleAddress1[0] && scanResults[ii].address[1] == bleAddress1[1] 
      && scanResults[ii].address[2] == bleAddress1[2] && scanResults[ii].address[3] == bleAddress1[3] 
      && scanResults[ii].address[4] == bleAddress1[4] && scanResults[ii].address[5] == bleAddress1[5]){// || scanResults[ii].address[jj] == bleAddress2[jj]  || scanResults[ii].address[jj] == bleAddress3[jj]){
        Serial.printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X ",scanResults[ii].address[0], scanResults[ii].address[1], scanResults[ii].address[2],
        scanResults[ii].address[3], scanResults[ii].address[4], scanResults[ii].address[5]);
        Serial.printlnf("Found ICE RSSI : %d", scanResults[ii].rssi);
      }
      if(scanResults[ii].address[0] == bleAddress2[0] && scanResults[ii].address[1] == bleAddress2[1] 
      && scanResults[ii].address[2] == bleAddress2[2] && scanResults[ii].address[3] == bleAddress2[3] 
      && scanResults[ii].address[4] == bleAddress2[4] && scanResults[ii].address[5] == bleAddress2[5]){
        Serial.printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X ",scanResults[ii].address[0], scanResults[ii].address[1], scanResults[ii].address[2],
        scanResults[ii].address[3], scanResults[ii].address[4], scanResults[ii].address[5]);
        Serial.printlnf("Found COCONUT RSSI : %d", scanResults[ii].rssi);

      }
      if(scanResults[ii].address[0] == bleAddress3[0] && scanResults[ii].address[1] == bleAddress3[1] 
      && scanResults[ii].address[2] == bleAddress3[2] && scanResults[ii].address[3] == bleAddress3[3] 
      && scanResults[ii].address[4] == bleAddress3[4] && scanResults[ii].address[5] == bleAddress3[5]){
        Serial.printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X ",scanResults[ii].address[0], scanResults[ii].address[1], scanResults[ii].address[2],
        scanResults[ii].address[3], scanResults[ii].address[4], scanResults[ii].address[5]);
        Serial.printlnf("Found MINT RSSI : %d", scanResults[ii].rssi);

      }
      else{
        //Serial.printlnf("not found");
      }
    }
  //}
    delay(4000);
  }
  
