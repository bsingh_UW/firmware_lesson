/*
 * Project BoronUARTBle
 * Description:
 * Author:
 * Date:
 */

#include "Particle.h"

// This example does not require the cloud so you can run it in manual mode or
// normal cloud-connected mode
// SYSTEM_MODE(MANUAL);

const size_t UART_TX_BUF_SIZE = 20;

const unsigned long UPDATE_INTERVAL_MS = 2000;
unsigned long lastUpdate = 0;

void onDataReceived(const uint8_t* data, size_t len, const BlePeerDevice& peer, void* context);

// These UUIDs were defined by Nordic Semiconductor and are now the defacto standard for
// UART-like services over BLE. Many apps support the UUIDs now, like the Adafruit Bluefruit app.
const BleUuid serviceUuid("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
const BleUuid rxUuid("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
const BleUuid txUuid("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");

//Create BleCharacteristic object
BleCharacteristic txCharacteristic("tx", BleCharacteristicProperty::NOTIFY, txUuid, serviceUuid);
BleCharacteristic rxCharacteristic("rx", BleCharacteristicProperty::WRITE_WO_RSP, rxUuid, serviceUuid, onDataReceived, NULL);

//Function that is called when data is received
void onDataReceived(const uint8_t* data, size_t len, const BlePeerDevice& peer, void* context) {
    //Log.trace("Received data from: %02X:%02X:%02X:%02X:%02X:%02X:", peer.address()[0], peer.address()[1], peer.address()[2], peer.address()[3], peer.address()[4], peer.address()[5]);
    //Write the recived data to the serial monitor
    for (size_t ii = 0; ii < len; ii++) {
        Serial.write(data[ii]);
    }
    Serial.println();
}

void setup() {
    Serial.begin();
    
    BLE.on();

    BLE.addCharacteristic(txCharacteristic);
    BLE.addCharacteristic(rxCharacteristic);

    BleAdvertisingData data;
    data.appendServiceUUID(serviceUuid);
    BLE.advertise(&data);
}

void loop() {
    if (millis() - lastUpdate >= UPDATE_INTERVAL_MS)
    {
        lastUpdate = millis();
        if (BLE.connected()) {
            uint8_t txBuf[UART_TX_BUF_SIZE];
            size_t txLen = 0;
            //Sending data
            String myString = "Sending hello";
                    
            for (size_t i = 0; i < myString.length() ; i++){
                txBuf[txLen++] = myString[i];
                Serial.write(txBuf[txLen - 1]);
            }
            Serial.println();
                
            if (txLen > 0) {
                txCharacteristic.setValue(txBuf, txLen);
        
            }
        }
    }
}
